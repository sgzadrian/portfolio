export const state = () => ({
  theme: true,
})

export const getters = {
  theme: ( state ) => { return state.theme },
}

export const mutations = {
  toggleTheme ( state ) {
    state.theme = !state.theme
  }
}

export const actions = {
}

export const modules = {
}
