export const state = () => ({
  all: [{
    image: 'nuvola',
    title: 'Nuvola Mom',
    desc: 'Ionic App, API, IoT Device, Landing Page & Blog',
    techs: [
      'fab fa-google',
      'fab fa-laravel',
      'fab fa-angular',
      'fab fa-android',
      'fab fa-apple',
      'fab fa-wordpress-simple',
    ],
    link: null,
  }, {
    image: 'mayorquin',
    title: 'Mayorquin Joyerias',
    desc: 'Ecommerce - API, Payments, Dashboard & Web',
    techs: [ 'fab fa-node-js', 'fab fa-laravel', 'fab fa-angular', 'fab fa-vuejs', 'fab fa-sass' ],
    link: 'https://cachasmayorquin.mx',
  }, {
    image: 'nowly',
    title: 'Nowly',
    desc: 'Course Platform - API, Video Processing, Payments, Dashboard & Web',
    techs: [ 'fab fa-google', 'fab fa-laravel', 'fab fa-angular', 'fab fa-vuejs', 'fab fa-sass' ],
    link: 'https://gonowly.com',
  }, {
    image: '40db',
    title: '40 Decibeles Podcast',
    desc: 'Podcast Website - API, Dash & Web',
    techs: [ 'fab fa-aws', 'fab fa-laravel', 'fab fa-angular', 'fab fa-vuejs', 'fab fa-sass' ],
    link: 'https://40decibeles.com',
  }, {
    image: 'chapa',
    title: 'Chapa Cardozo',
    desc: 'Ionic App - API, Dash & Mobile App',
    techs: [ 'fab fa-google', 'fab fa-laravel', 'fab fa-angular', 'fab fa-android', 'fab fa-apple' ],
    link: 'https://apps.apple.com/us/app/chapa-cardozo/id1565855831',
  }, {
    image: 'anglu',
    title: 'Anglu Studio',
    desc: 'Portfolio Website - API, Dash & Web',
    techs: [ 'fab fa-laravel', 'fab fa-angular', 'fab fa-vuejs', 'fab fa-sass' ],
    link: 'https://anglu.altien.mx',
  }, {
    image: 'abrelatas',
    title: 'Abrelatas',
    desc: 'Portfolio Website - API, Dash & Web',
    techs: [ 'fab fa-laravel', 'fab fa-angular', 'fab fa-sass' ],
    link: 'https://abrelatas.com.mx',
  }, {
    image: 'newmix',
    title: 'NewMix',
    desc: 'Contest Website - API, Web & Support',
    techs: [ 'fab fa-laravel', 'fab fa-angular', 'fab fa-sass' ],
    link: 'https://newmix.hellcoders.com/',
  }, {
    image: 'jimador',
    title: 'El Jimador',
    desc: 'Contest Website - API, Web & Support',
    techs: [ 'fab fa-laravel', 'fab fa-angular', 'fab fa-sass' ],
    link: 'https://jimador.hellcoders.com/',
  }, {
    image: 'skinwit',
    title: 'SkinWit',
    desc: 'Ecommerce - Design, Styling & Support',
    techs: [ 'fab fa-wordpress-simple', 'fab fa-sass' ],
    link: 'https://skinwit.com',
  }, {
    image: 'cetiapp',
    title: 'CETI App',
    desc: 'Ionic App - API, Dash & Mobile App',
    techs: [ 'fab fa-google', 'fab fa-laravel', 'fab fa-angular', 'fab fa-android', 'fab fa-apple' ],
    link: null,
  }, {
    image: 'areli',
    title: 'Arelí Aréchiga',
    desc: 'Ecommerce & Blog - Design, Styling & Support',
    techs: [ 'fab fa-wordpress-simple', 'fab fa-sass' ],
    link: 'https://dearmebyareli.com',
  }, {
    image: 'rossoamore',
    title: 'Rosso Amore',
    desc: 'Ecommerce - Design, Styling & Support',
    techs: [ 'fab fa-wordpress-simple', 'fab fa-sass' ],
    link: 'https://rossoamore.com.mx',
  }, {
    image: 'sandoval-lp',
    title: 'Sandoval Lopez Peña Abogados',
    desc: 'Website & Blog',
    techs: [ 'fab fa-laravel', 'fab fa-wordpress-simple', 'fab fa-vuejs', 'fab fa-sass' ],
    link: 'https://sandovalplabogados.com',
  }, {
    image: 'elite',
    title: 'Elite Empresarial',
    desc: 'Landing Page',
    techs: [ 'fab fa-angular', 'fab fa-sass' ],
    link: 'https://eliteempresarial.com.mx',
  }, {
    image: 'monolito',
    title: 'Monolito Estudio',
    desc: 'Landing Page',
    techs: [ 'fab fa-wordpress-simple', 'fab fa-sass' ],
    link: 'https://monolitoestudio.com',
  }, {
    // image: 'dynami',
    // title: 'Dynami Rehabilitación',
    // desc: '',
    // techs: [],
    // link: 'https://dynamirehabilitacion.com',
  // }, {
    image: 'my-barber',
    title: 'MyBarber',
    desc: 'Ionic App - API, Dash & Mobile App',
    techs: [ 'fab fa-laravel', 'fab fa-angular', 'fab fa-android', 'fab fa-apple' ],
    link: null,
  }],
})

export const getters = {
  all: ( state ) => { return state.all },
}

export const mutations = {
}

export const actions = {
}

export const modules = {
}
