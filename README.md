[![Portfolio](https://gitlab.com/sgzadrian/portfolio/raw/master/images/portfolio.jpg?inline=false)](https://sgzadrian.gitlab.io/portfolio)

Portfolio created mainly to share some of my daily work as software developer. It fetches data from a custom API and all the images had been created by me (except for the angular and the laravel logo).
In this project you can find REST API data load, responsive desing and some pretty basic skills of graphic design.
Any sugestion, comment, improvement or critic is well received, feel free to send me a [mail](mailto:sgzadrian@gmail.com).

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
