import axios from 'axios'
import VInput from '../vinput/vinput.vue'

export default {
  components: {
    VInput
  },

  data () {
    return {

      sending: false,
      contactError: false,
      errorMail: 'mailto:contacto@hellcoders.com.mx:?subject=Falló el formulario&body=',

      contactForm: {
        name: '',
        email: '',
        message: '',
        validation: ''
      }

    }
  },

  methods: {

    sendContact () {
      if ( this.contactForm.validation.length != 0 ) { return }
      this.sending = true
      axios.post( process.env.API_URL + '/mail', this.contactForm )
        .then(() => {
          window.location.href = '/thanks'
        })
        .catch((err) => {
          console.error('Error ', err)
          this.contactError = true
          this.sending = false
        })
    }
  }
}
