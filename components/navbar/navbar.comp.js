export default {
  data () {
    return {
      EMAIL: 'sgzadrian@gmail.com',
      GIT_URL: 'https://gitlab.com/sgzadrian',
      LINKEDIN_URL: 'https://linkedin.com/in/sgzadrian',
    }
  },

  computed: {
    isDark() { return this.$store.getters.theme }
  },

  methods: {
    toggleTheme() {
      this.$store.commit( 'toggleTheme' )
    }
  }
}
