export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ssr: true,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Adrián Sánchez | Software Engineer',
    meta: [
      { lang: 'es' },
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Desarrollador de Software en Guadalajara Jalisco'
      },
      { name: 'author', content: 'sgzadrian' },
      { name: 'msapplication-TileColor', content: '#000000' },
      { name: 'msapplication-config', content: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/browserconfig.xml' },
      { name: 'theme-color', content: '#000000' },
      // Facebook Meta
      { property: 'og:url', content: 'https://sgzadrian.gitlab.io/portfolio' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'Adrián Sánchez | Software Engineer' },
      { property: 'og:image', content: 'https://sgzadrian.gitlab.io/portfolio/images/social.png' },
      {
        property: 'og:description',
        content: 'Desarrollador de Software en Guadalajara Jalisco'
      },
      // Twitter Meta
      { name: 'twitter:card', content: 'summary' },
      { name: 'twitter:title', content: 'Adrián Sánchez | Software Engineer' },
      { property: 'twitter:image', content: 'https://sgzadrian.gitlab.io/portfolio/images/social.png' },
      {
        name: 'twitter:description',
        content: 'Desarrollador de Software en Guadalajara Jalisco'
      }
    ],
    link: [
      // { rel: 'canonical', href: 'https://sgzadrian.gitlab.io/portfolio/index.html' },
      // Icons
      { rel: 'icon', type: 'image/x-icon', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/favicon-16x16.png' },
      { rel: 'mask-icon', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/safari-pinned-tab.svg', color: '#E44444' },
      { rel: 'shortcut icon', href: 'https://sgzadrian.gitlab.io/portfolio/assets/favicons/favicon.ico' },
      // Preloaders
      { rel: 'preload', as: 'script', href: 'https://kit.fontawesome.com/2c6b1af076.js', crossorigin: 'anonymous' },
      { rel: 'preload', as: 'script', href: 'https://www.googletagmanager.com/gtag/js?id=G-E729RP4EPT', crossorigin: 'anonymous' },
      // Preconnections
      { rel: 'preconnect', href: 'https://fonts.googleapis.com', crossorigin: 'anonymous' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: 'anonymous' },
      // DNS Prefetchers
      { rel: 'dns-prefetch', href: 'https://www.googletagmanager.com/gtag/js', crossorigin: 'anonymous' },
      // { rel: 'dns-prefetch', href: 'https://www.googletagmanager.com', crossorigin: 'anonymous' },
      // { rel: 'dns-prefetch', href: 'https://www.facebook.com', crossorigin: 'anonymous' },
      // { rel: 'dns-prefetch', href: 'https://connect.facebook.net', crossorigin: 'anonymous' },
      // Ionic Iocons
      { rel: 'stylesheet', href: 'https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css' },
      // Google Fonts
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap' }
    ],
    script: [
      { src: 'https://kit.fontawesome.com/2c6b1af076.js', crossorigin: 'anonymous' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#E44444' },
  /*
   ** Global CSS
   */
  css: [
    '@/styles.scss'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    '@nuxtjs/google-gtag'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
  },
  /*
   ** Env configuration
   */
  dotenv: {
    filename: process.env.NODE_ENV == 'production' ? '.env.prod' : '.env',
  },
  /*
   ** Customize the base url
   */
  router: {
    base: process.env.NODE_ENV == 'production' ? '/portfolio/' : '/'
  },
  /* Google Gtag conf */
  'google-gtag': {
    id: 'G-E729RP4EPT',
    config: {
      anonymize_ip: true,
      send_page_view: false,
    },
    debug: false,
    disableAutoPageTrack: false,
  },
  /*
   ** Ignore Files
   */
  ignore: [
    '**/*.test.*',
    '*.ignore.*',
    '*.comp.*'
  ],
  /*
   ** Generate configuration
   */
  generate: {
    dir: 'public',
    exclude: [
      /^(?=.*\bignore\b).*$/
    ]
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend (config, ctx) {
    }
  }
}
