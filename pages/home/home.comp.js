export default {
  components: {
  },

  data () {
    return {
      BASE_IMG: '/clients/',
    }
  },

  computed: {
    projects() { return this.$store.getters[ 'projects/all' ] },
    isDark() { return this.$store.getters.theme }
  },

  mounted() {
    let isProd = process.env.NODE_ENV == 'production'
    if ( isProd ) {
      this.BASE_IMG = '/portfolio' + this.BASE_IMG
    }
  },

  methods: {
  }
}
