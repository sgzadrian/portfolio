module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  // add your custom rules here
  rules: {
    'semi': 0,
    'eqeqeq': 0,
    'indent': [ 'off', 'ignoreComments' ],
    'no-console': 0,
    'key-spacing': 0,
    'comma-dangle': 0,
    'arrow-parens': 0,
    'prefer-const': 0,
    'padded-blocks': 0,
    'no-unreachable': 0,
    'no-multi-spaces': 0,
    'space-in-parens': 0,
    'space-infix-ops': 0,
    'array-bracket-spacing': 0,
    'space-before-function-paren': 0,
    'computed-property-spacing': 0,
    'template-curly-spacing': 0,
    'vue/comment-directive': 0,
    'vue/require-default-prop': 0,
    'vue/require-prop-types': 0,
    'vue/html-self-closing': 0,
  }
}
